package com.blogspot.codingandmore;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Vector;

import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.installer.ArtifactInstallationException;
import org.apache.maven.artifact.installer.ArtifactInstaller;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * this mojo is used to deploy local jboss libraries to the local repository
 * 
 * @author wohlgemuth
 * @goal deploy
 * @phase process-resources
 * @requiresProject false
 */
public class DeployMojo extends AbstractMojo {

	/** @parameter default-value="${localRepository}" */
	private org.apache.maven.artifact.repository.ArtifactRepository localRepository;

	/** @component */
	private ArtifactInstaller installer;

	public String getBaseGroup() {
		return baseGroup;
	}

	public void setBaseGroup(String baseGroup) {
		this.baseGroup = baseGroup;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * where is our jboss home located
	 * 
	 * @parameter expression="${jbossHome}"
	 */
	private File jbossHome;

	/**
	 * defined base group where we store the jars
	 */
	private String baseGroup = "com.blogspot.codingandmore.jboss";

	public org.apache.maven.artifact.repository.ArtifactRepository getLocalRepository() {
		return localRepository;
	}

	public void setLocalRepository(
			org.apache.maven.artifact.repository.ArtifactRepository localRepository) {
		this.localRepository = localRepository;
	}

	public ArtifactInstaller getInstaller() {
		return installer;
	}

	public void setInstaller(ArtifactInstaller installer) {
		this.installer = installer;
	}

	public File getJbossHome() {
		return jbossHome;
	}

	public void setJbossHome(File jbossHome) {
		this.jbossHome = jbossHome;
	}

	public Vector<String> getJbossConfigurations() {
		return jbossConfigurations;
	}

	public void setJbossConfigurations(Vector<String> jbossConfigurations) {
		this.jbossConfigurations = jbossConfigurations;
	}

	/**
	 * contains our configurations
	 * 
	 * @parameter expression="${jbossConfigurations}"
	 */
	private Vector<String> jbossConfigurations = new Vector<String>();

	/**
	 * contains our configurations
	 * 
	 * @parameter expression="${version}"
	 * @required
	 */
	private String version = null;

	public void execute() throws MojoExecutionException, MojoFailureException {

		if (jbossHome == null) {
			jbossHome = new File(System.getProperty("user.home")
					+ File.separator + "jboss");
			getLog().debug(
					"setting jboss home to: " + jbossHome.getAbsolutePath());
		}
		if (jbossHome.exists() == false) {
			jbossHome = new File("/opt/jboss");
			getLog().debug(
					"setting jboss home to: " + jbossHome.getAbsolutePath());

		}
		if (jbossHome.exists() == false) {
			jbossHome = new File("/usr/jboss");
			getLog().debug(
					"setting jboss home to: " + jbossHome.getAbsolutePath());

		}
		if (jbossHome.exists() == false) {
			jbossHome = new File("/usr/local/jboss");
			getLog().debug(
					"setting jboss home to: " + jbossHome.getAbsolutePath());

		}

		if (jbossHome.exists() == false) {
			throw new MojoExecutionException(
					"giving up, we can't find jboss home, why don't you specify it your self using the flag '-DjbossHome=<PATH>'");
		}

		// our jboss configurations
		if (jbossConfigurations.isEmpty()) {
			// if it's empty we need to populate it
			File file = new File(jbossHome.getAbsolutePath() + File.separator
					+ "server");

			for (String f : file.list()) {
				getLog().debug("adding configuration: " + f);
				this.getJbossConfigurations().add(f);
			}
		}

		try {
			getLog().info("working on configurations");
			for (String s : this.getJbossConfigurations()) {
				File directory = new File(jbossHome.getAbsolutePath()
						+ File.separator + "server" + File.separator + s
						+ File.separator + "lib");

				File[] content = findJars(directory);

				for (File f : content) {
					handleFile(f, s);
				}
			}

			getLog().info("workig on client directory");
			File directory = new File(jbossHome.getAbsolutePath()
					+ File.separator + "client");

			File[] content = findJars(directory);
			for (File f : content) {
				handleFile(f, "client");
			}
		} catch (Exception e) {
			getLog().error(e);
			throw new MojoExecutionException(e.getMessage(),e);
		}
	}

	private void handleFile(File f, String suffix) throws IOException,
			ArtifactInstallationException, MojoExecutionException {
		DefaultArtifactHandler handler = new DefaultArtifactHandler("jar");
		DefaultArtifact artifact = new DefaultArtifact(this.baseGroup + "-"
				+ suffix, f.getName().replace(".jar", ""), VersionRange
				.createFromVersion(version ), null, "jar", null, handler);
		artifact.setFile(f);

		getLog().debug("installing: " + artifact.getArtifactId() + " - " + artifact.getGroupId() + " - " + artifact.getVersion());
	
		if(localRepository == null){
			throw new MojoExecutionException("no local repository was specified!!!");
		}
		this.installer.install(f, artifact, localRepository);
	}

	/**
	 * finds all jar files for us in the directory
	 * 
	 * @param directory
	 * @return
	 */
	private File[] findJars(File directory) {
		File[] content = directory.listFiles(new FileFilter() {

			public boolean accept(File pathname) {
				return pathname.getName().endsWith("jar");
			}
		});
		getLog().info("found libs: " + content.length);

		return content;
	}

}
