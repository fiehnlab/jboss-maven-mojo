package com.blogspot.codingandmore;

import java.io.File;

import org.apache.maven.artifact.repository.DefaultArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;

/**
 * tests our deploy mojo
 * @author wohlgemuth
 *
 */
public class DeployMojoTest extends AbstractMojoTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testExecute() throws Exception {
		final File baseDirectory = new File(getBasedir());

		final File testResourcesDirectory = new File(baseDirectory, "src/test/resources");

		DeployMojo mojo = (DeployMojo) lookupMojo("deploy",new File(testResourcesDirectory,"deploy.xml"));
		assertNotNull(mojo);
		final DefaultArtifactRepository repository = new DefaultArtifactRepository(
				"a", "a", new DefaultRepositoryLayout());

		repository.setBasedir(getBasedir()+"/target/repository");
		
		mojo.setLocalRepository(repository);
		mojo.execute();
	
		assertTrue(mojo.getVersion().equals("4.3.0Sp1"));
		assertTrue(mojo.getJbossConfigurations().size() == 3);
		assertTrue(new File(getBasedir()+"/target/repository").exists());
		
	}

}
